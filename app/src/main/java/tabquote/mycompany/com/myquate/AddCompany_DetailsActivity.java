package tabquote.mycompany.com.myquate;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by kveldurty on 12/26/16.
 */

public class AddCompany_DetailsActivity extends Activity implements View.OnClickListener{
    Button regcompbutton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addcompany_details_layout);
        regcompbutton = (Button) findViewById(R.id.regcompbutton);
        regcompbutton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        Intent intent = new Intent(this, ListActivity.class);
        startActivity(intent);

    }
}
