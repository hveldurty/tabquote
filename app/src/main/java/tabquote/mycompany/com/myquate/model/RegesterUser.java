package tabquote.mycompany.com.myquate.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class RegesterUser implements Parcelable {

	@SerializedName("service_id")
	@Expose
	private String serviceId;
	@SerializedName("sub_service_id")
	@Expose
	private String subServiceId;
	@SerializedName("details")
	@Expose
	private UserDetails details;
	public RegesterUser(){

	}
	protected RegesterUser(Parcel in) {
		serviceId = in.readString();
		subServiceId = in.readString();
		details = in.readParcelable(UserDetails.class.getClassLoader());
	}

	public static final Creator<RegesterUser> CREATOR = new Creator<RegesterUser>() {
		@Override
		public RegesterUser createFromParcel(Parcel in) {
			return new RegesterUser(in);
		}

		@Override
		public RegesterUser[] newArray(int size) {
			return new RegesterUser[size];
		}
	};

	/**
	 * 
	 * @return The serviceId
	 */
	public String getServiceId() {
		return serviceId;
	}

	/**
	 * 
	 * @param serviceId
	 *            The service_id
	 */
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	/**
	 * 
	 * @return The subServiceId
	 */
	public String getSubServiceId() {
		return subServiceId;
	}

	/**
	 * 
	 * @param subServiceId
	 *            The sub_service_id
	 */
	public void setSubServiceId(String subServiceId) {
		this.subServiceId = subServiceId;
	}

	/**
	 * 
	 * @return The details
	 */
	public UserDetails getDetails() {
		return details;
	}

	/**
	 * 
	 * @param details
	 *            The details
	 */
	public void setDetails(UserDetails details) {
		this.details = details;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(serviceId);
		parcel.writeString(subServiceId);
		parcel.writeParcelable(details, i);
	}
}