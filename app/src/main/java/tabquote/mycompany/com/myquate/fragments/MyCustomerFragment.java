package tabquote.mycompany.com.myquate.fragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.BasicStroke;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import tabquote.mycompany.com.myquate.BaseFragment;
import tabquote.mycompany.com.myquate.R;
import tabquote.mycompany.com.myquate.adapters.AlphabetListAdapter;
import tabquote.mycompany.com.myquate.model.CustomerModel;

/**
 * Created by M8080375 on 9/26/2016.
 */

public class MyCustomerFragment extends BaseFragment {
    public static final String ARG_ITEM_ID = "item_id";
    private AlphabetListAdapter adapter = new AlphabetListAdapter();
    private GestureDetector mGestureDetector;
    private List<Object[]> alphabet = new ArrayList<Object[]>();
    private HashMap<String, Integer> sections = new HashMap<String, Integer>();
    private int sideIndexHeight;
    private static float sideIndexX;
    private static float sideIndexY;
    private int indexListSize;
    // Search EditText
    private EditText inputSearch;
    ArrayList<CustomerModel> customerModels;
    private LinearLayout sideIndex;
    private ListView mListCustom;
    private LinearLayout chartContainer;
    private GraphicalView mChart;
    private XYMultipleSeriesDataset mDataset = new XYMultipleSeriesDataset();
    private XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
    private XYSeries mCurrentSeries;
    private XYSeriesRenderer mCurrentRenderer;
    private String[] mMonth = new String[] {
            "Jan", "Feb" , "Mar", "Apr", "May", "Jun",
            "Jul", "Aug" , "Sep", "Oct", "Nov", "Dec"
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflateAndBind(inflater, container, R.layout.customer_main_layout);
        sideIndex = (LinearLayout) rootView.findViewById(R.id.sideIndex);
        chartContainer = (LinearLayout) rootView.findViewById(R.id.chart);
        inputSearch = (EditText) rootView.findViewById(R.id.inputSearch);
        //mGestureDetector = new GestureDetector(this, new ListActivity().SideIndexGestureListener());
        mListCustom = (ListView) rootView.findViewById(R.id.side_list_cust);
        customerModels =populateCountries();
        //List<String> countries = populateCountries();
        Collections.sort(customerModels);

        ArrayList<AlphabetListAdapter.Row> rows = new ArrayList<AlphabetListAdapter.Row>();
        int start = 0;
        int end = 0;
        String previousLetter = null;
        Object[] tmpIndexItem = null;
        Pattern numberPattern = Pattern.compile("[0-9]");
        openChart();

        for (CustomerModel customer : customerModels) {
            String firstLetter = customer.getCompName().substring(0, 1);

            // Group numbers together in the scroller
            if (numberPattern.matcher(firstLetter).matches()) {
                firstLetter = "#";
            }

            // If we've changed to a new letter, add the previous letter to the alphabet scroller
            if (previousLetter != null && !firstLetter.equals(previousLetter)) {
                end = rows.size() - 1;
                tmpIndexItem = new Object[3];
                tmpIndexItem[0] = previousLetter.toUpperCase(Locale.UK);
                tmpIndexItem[1] = start;
                tmpIndexItem[2] = end;
                alphabet.add(tmpIndexItem);

                start = end + 1;
            }

            // Check if we need to add a header row
            if (!firstLetter.equals(previousLetter)) {
                rows.add(new AlphabetListAdapter.Section(firstLetter));
                sections.put(firstLetter, start);
            }

            // Add the country to the list
            rows.add(new AlphabetListAdapter.Item(customer));
            previousLetter = firstLetter;
        }

        if (previousLetter != null) {
            // Save the last letter
            tmpIndexItem = new Object[3];
            tmpIndexItem[0] = previousLetter.toUpperCase(Locale.UK);
            tmpIndexItem[1] = start;
            tmpIndexItem[2] = rows.size() - 1;
            alphabet.add(tmpIndexItem);
        }

        adapter.setRows(rows);
        mListCustom.setAdapter(adapter);

        updateList();
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                filterData(cs.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });


        return rootView;
    }
    public static MyCustomerFragment newInstance(String itemID) {
        MyCustomerFragment fragment = new MyCustomerFragment();
        Bundle args = new Bundle();
        args.putString(MyCustomerFragment.ARG_ITEM_ID, itemID);
        fragment.setArguments(args);
        return fragment;
    }
    class SideIndexGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            sideIndexX = sideIndexX - distanceX;
            sideIndexY   = sideIndexY - distanceY;

            if (sideIndexX >= 0 && sideIndexY >= 0) {
                displayListItem();
            }

            return super.onScroll(e1, e2, distanceX, distanceY);
        }
    }
    public void updateList() {
       // LinearLayout sideIndex = (LinearLayout) findViewById(R.id.sideIndex);


    }

    public void displayListItem() {
    /*    sideIndexHeight = sideIndex.getHeight();
        // compute number of pixels for every side index item
        double pixelPerIndexItem = (double) sideIndexHeight / indexListSize;

        // compute the item index for given event position belongs to
        int itemPosition = (int) (sideIndexY / pixelPerIndexItem);

        // get the item (we can do it since we know item index)
        if (itemPosition < alphabet.size()) {
            Object[] indexItem = alphabet.get(itemPosition);
            int subitemPosition = sections.get(indexItem[0]);

            //ListView listView = (ListView) findViewById(android.R.id.list);
           // getActivity().getListView().setSelection(subitemPosition);
        }*/
    }

    private ArrayList<CustomerModel> populateCountries() {
        ArrayList<CustomerModel> customerModels = new ArrayList<CustomerModel>();
        CustomerModel cust= new CustomerModel();
        cust.setCompName("Afghanistan");
        customerModels.add(cust);
        CustomerModel cust1= new CustomerModel();
        cust1.setCompName("Albania");
        customerModels.add(cust1);
        CustomerModel cust2= new CustomerModel();
        CustomerModel cust3= new CustomerModel();
        CustomerModel cust4= new CustomerModel();
        CustomerModel cust5= new CustomerModel();
        CustomerModel cust6= new CustomerModel();
        CustomerModel cust7= new CustomerModel();
        CustomerModel cust8= new CustomerModel();
        CustomerModel cust9= new CustomerModel();
        CustomerModel cust10= new CustomerModel();
        CustomerModel cust11= new CustomerModel();
        CustomerModel cust12= new CustomerModel();
        CustomerModel cust13= new CustomerModel();
        CustomerModel cust14= new CustomerModel();
        CustomerModel cust15= new CustomerModel();
        CustomerModel cust16= new CustomerModel();
        CustomerModel cust19= new CustomerModel();
        CustomerModel cust20= new CustomerModel();
        CustomerModel cust21= new CustomerModel();
        CustomerModel cust22= new CustomerModel();
        CustomerModel cust23= new CustomerModel();

        CustomerModel cust17= new CustomerModel();

        CustomerModel cust18= new CustomerModel();

        cust2.setCompName("Bahrain");
        customerModels.add(cust2);
        cust3.setCompName("Bangladesh");
        customerModels.add(cust3);
        cust4.setCompName("Cambodia");
        customerModels.add(cust4);
        cust5.setCompName("Albania");

        customerModels.add(cust5);
        cust6.setCompName("Cameroon");
        customerModels.add(cust6);
        cust7.setCompName("Denmark");
        customerModels.add(cust7);
        cust8.setCompName("Djibouti");
        customerModels.add(cust8);
        cust9.setCompName("East Timor");
        customerModels.add(cust9);
        cust10.setCompName("Ecuador");
        customerModels.add(cust10);
        cust11.setCompName("Fiji");
        customerModels.add(cust11);
        cust12.setCompName("Finland");
        customerModels.add(cust12);
        cust13.setCompName("Gabona");
        customerModels.add(cust13);
        cust14.setCompName("Kazakhstan");
        customerModels.add(cust14);
        cust15.setCompName("Macau");
        customerModels.add(cust15);
        cust16.setCompName("Holy See");
        customerModels.add(cust16);
        cust17.setCompName("Georgia");
        customerModels.add(cust17);
        cust18.setCompName("Iceland");
        customerModels.add(cust18);
        cust19.setCompName("Namibia");
        customerModels.add(cust19);
        cust20.setCompName("Haiti");
        customerModels.add(cust20);
        cust21.setCompName("Kenya");
        customerModels.add(cust21);
        cust22.setCompName("Taiwan");
        customerModels.add(cust22);
        cust23.setCompName("Laos");
        customerModels.add(cust23);
        return customerModels;
    }
    /**
     * According to the values in the input box to filter the data and update ListView
     * @param filterStr
     */
    private void filterData(String filterStr) {
        List<CustomerModel> filterDateList = new ArrayList<CustomerModel>();

        if (TextUtils.isEmpty(filterStr)) {
            filterDateList = customerModels;
        } else {
            filterDateList.clear();
            for (CustomerModel sortModel : customerModels) {
                String name = sortModel.getCompName();
                if (name.toUpperCase().indexOf(
                        filterStr.toString().toUpperCase()) != -1
                        || getSelling(name).toUpperCase()
                        .startsWith(filterStr.toString().toUpperCase())) {
                    filterDateList.add(sortModel);
                }
            }
        }

        Collections.sort(filterDateList);

        ArrayList<AlphabetListAdapter.Row> rows = new ArrayList<AlphabetListAdapter.Row>();
        int start = 0;
        int end = 0;
        String previousLetter = null;
        Object[] tmpIndexItem = null;
        Pattern numberPattern = Pattern.compile("[0-9]");

        for (CustomerModel customer : filterDateList) {
            String firstLetter = customer.getCompName().substring(0, 1);

            // Group numbers together in the scroller
            if (numberPattern.matcher(firstLetter).matches()) {
                firstLetter = "#";
            }

            // If we've changed to a new letter, add the previous letter to the alphabet scroller
            if (previousLetter != null && !firstLetter.equals(previousLetter)) {
                end = rows.size() - 1;
                tmpIndexItem = new Object[3];
                tmpIndexItem[0] = previousLetter.toUpperCase(Locale.UK);
                tmpIndexItem[1] = start;
                tmpIndexItem[2] = end;
                alphabet.add(tmpIndexItem);

                start = end + 1;
            }

            // Check if we need to add a header row
            if (!firstLetter.equals(previousLetter)) {
                rows.add(new AlphabetListAdapter.Section(firstLetter));
                sections.put(firstLetter, start);
            }

            // Add the country to the list
            rows.add(new AlphabetListAdapter.Item(customer));
            previousLetter = firstLetter;
        }

        if (previousLetter != null) {
            // Save the last letter
            tmpIndexItem = new Object[3];
            tmpIndexItem[0] = previousLetter.toUpperCase(Locale.UK);
            tmpIndexItem[1] = start;
            tmpIndexItem[2] = rows.size() - 1;
            alphabet.add(tmpIndexItem);
        }

        adapter.setRows(rows);
        mListCustom.setAdapter(adapter);

        updateList();
    }

    private StringBuilder buffer;
    /** * Chinese characters are converted to ASCII code * * @param chs * @return */
    private int getChsAscii(String chs) {
        int asc = 0;
        try {
            byte[] bytes = chs.getBytes("gb2312");
            if (bytes == null || bytes.length > 2 || bytes.length <= 0) {
                throw new RuntimeException("illegal resource string");
            }
            if (bytes.length == 1) {
                asc = bytes[0];
            }
            if (bytes.length == 2) {
                int hightByte = 256 + bytes[0];
                int lowByte = 256 + bytes[1];
                asc = (256 * hightByte + lowByte) - 256 * 256;
            }
        } catch (Exception e) {
            System.out.println("ERROR:ChineseSpelling.class-getChsAscii(String chs)" + e);
        }
        return asc;
    }


    /** * The phrase analysis * * @param chs * @return */
    public String getSelling(String chs) {
        String key, value;
        buffer = new StringBuilder();
        for (int i = 0; i <chs.length(); i++) {
            key = chs.substring(i, i + 1);
         /*   if (key.getBytes().length >= 2) {
                value = (String) convert(key);
                if (value == null) {
                    value = "unknown";
                }
            } else {
                value = key;
            }*/
            buffer.append(key);
        }
        return buffer.toString();
    }
    /* Chart related code*/
    private void openChart(){
        int[] x = { 0,1,2,3,4,5,6,7, 8, 9, 10, 11 };
        int[] income = { 2000,2500,2700,3000,2800,3500,3700,3800, 0,0,0,0};
        int[] expense = {2200, 2700, 2900, 2800, 2600, 3000, 3300, 3400, 0, 0, 0, 0 };

// Creating an XYSeries for Income
        XYSeries incomeSeries = new XYSeries("Income");
// Creating an XYSeries for Expense
        XYSeries expenseSeries = new XYSeries("Expense");
// Adding data to Income and Expense Series
        for(int i=0;i<x.length;i++){
            incomeSeries.add(i,income[i]);
            expenseSeries.add(i,expense[i]);
        }
        // Creating a dataset to hold each series
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
// Adding Income Series to the dataset
        dataset.addSeries(incomeSeries);
// Adding Expense Series to dataset
        dataset.addSeries(expenseSeries);

// Creating XYSeriesRenderer to customize incomeSeries
        XYSeriesRenderer incomeRenderer = new XYSeriesRenderer();

        incomeRenderer.setColor(Color.CYAN); //color of the graph set to cyan
        incomeRenderer.setFillPoints(true);
        incomeRenderer.setLineWidth(2f);
        incomeRenderer.setDisplayChartValues(true);
//setting chart value distance
        incomeRenderer.setDisplayChartValuesDistance(10);
//setting line graph point style to circle
        incomeRenderer.setPointStyle(PointStyle.CIRCLE);
//setting stroke of the line chart to solid
        incomeRenderer.setStroke(BasicStroke.SOLID);

// Creating XYSeriesRenderer to customize expenseSeries
        XYSeriesRenderer expenseRenderer = new XYSeriesRenderer();
        expenseRenderer.setColor(Color.GREEN);
        expenseRenderer.setFillPoints(true);
        expenseRenderer.setLineWidth(2f);
        expenseRenderer.setDisplayChartValues(true);
//setting line graph point style to circle
        expenseRenderer.setPointStyle(PointStyle.SQUARE);
//setting stroke of the line chart to solid
        expenseRenderer.setStroke(BasicStroke.SOLID);

// Creating a XYMultipleSeriesRenderer to customize the whole chart
        XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
        multiRenderer.setXLabels(0);
        multiRenderer.setChartTitle("Income vs Expense Chart");
        multiRenderer.setXTitle("Year 2016");
        multiRenderer.setYTitle("Amount in Dollars");

/***
 * Customizing graphs
 */
//setting text size of the title
        multiRenderer.setChartTitleTextSize(28);
//setting text size of the axis title
        multiRenderer.setAxisTitleTextSize(24);
//setting text size of the graph lable
        multiRenderer.setLabelsTextSize(24);
//setting zoom buttons visiblity
        multiRenderer.setZoomButtonsVisible(false);
//setting pan enablity which uses graph to move on both axis
        multiRenderer.setPanEnabled(false, false);
//setting click false on graph
        multiRenderer.setClickEnabled(false);
//setting zoom to false on both axis
        multiRenderer.setZoomEnabled(false, false);
//setting lines to display on y axis
        multiRenderer.setShowGridY(true);
//setting lines to display on x axis
        multiRenderer.setShowGridX(true);
//setting legend to fit the screen size
        multiRenderer.setFitLegend(true);
//setting displaying line on grid
        multiRenderer.setShowGrid(true);
//setting zoom to false
        multiRenderer.setZoomEnabled(false);
//setting external zoom functions to false
        multiRenderer.setExternalZoomEnabled(false);
//setting displaying lines on graph to be formatted(like using graphics)
        multiRenderer.setAntialiasing(true);
//setting to in scroll to false
        multiRenderer.setInScroll(false);
//setting to set legend height of the graph
        multiRenderer.setLegendHeight(30);
//setting x axis label align
        multiRenderer.setXLabelsAlign(Paint.Align.CENTER);
//setting y axis label to align
        multiRenderer.setYLabelsAlign(Paint.Align.LEFT);
//setting text style
        multiRenderer.setTextTypeface("sans_serif", Typeface.NORMAL);
//setting no of values to display in y axis
        multiRenderer.setYLabels(10);
// setting y axis max value, Since i'm using static values inside the graph so i'm setting y max value to 4000.
// if you use dynamic values then get the max y value and set here
        multiRenderer.setYAxisMax(4000);
//setting used to move the graph on xaxiz to .5 to the right
        multiRenderer.setXAxisMin(-0.5);
//setting used to move the graph on xaxiz to .5 to the right
        multiRenderer.setXAxisMax(11);
//setting bar size or space between two bars
//multiRenderer.setBarSpacing(0.5);
//Setting background color of the graph to transparent
        multiRenderer.setBackgroundColor(Color.TRANSPARENT);
//Setting margin color of the graph to transparent
        multiRenderer.setMarginsColor(getResources().getColor(R.color.transparent_background));
        multiRenderer.setApplyBackgroundColor(true);
        multiRenderer.setScale(2f);
//setting x axis point size
        multiRenderer.setPointSize(4f);
//setting the margin size for the graph in the order top, left, bottom, right
        multiRenderer.setMargins(new int[]{30, 30, 30, 30});

        for(int i=0; i< x.length;i++){
            multiRenderer.addXTextLabel(i, mMonth[i]);
        }

// Adding incomeRenderer and expenseRenderer to multipleRenderer
// Note: The order of adding dataseries to dataset and renderers to multipleRenderer
// should be same
        multiRenderer.addSeriesRenderer(incomeRenderer);
        multiRenderer.addSeriesRenderer(expenseRenderer);

//this part is used to display graph on the xml

//remove any views before u paint the chart
        chartContainer.removeAllViews();
//drawing bar chart
        mChart = ChartFactory.getLineChartView(getActivity(), dataset, multiRenderer);
//adding the view to the linearlayout
        chartContainer.addView(mChart);

    }
}
