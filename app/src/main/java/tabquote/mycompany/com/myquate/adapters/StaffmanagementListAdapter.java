package tabquote.mycompany.com.myquate.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import tabquote.mycompany.com.myquate.R;
import tabquote.mycompany.com.myquate.model.QuotationModel;

/**
 * Created by kveldurty on 12/17/16.
 */

public class StaffmanagementListAdapter extends RecyclerView.Adapter<StaffmanagementListAdapter.CustViewAdapter> {

    private ArrayList<QuotationModel> quoteList;
    private Context mContext;
    public class CustViewAdapter extends RecyclerView.ViewHolder {
        public TextView username, assigned, completed;


        public CustViewAdapter(View view) {
            super(view);
            username = (TextView) view.findViewById(R.id.username);
            assigned = (TextView) view.findViewById(R.id.assigned);
            completed = (TextView) view.findViewById(R.id.completed);

        }
    }


    public StaffmanagementListAdapter(Context context, ArrayList<QuotationModel> custList) {
        this.quoteList = custList;
        this.mContext = context;
    }

    @Override
    public StaffmanagementListAdapter.CustViewAdapter onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.staff_listitem, parent, false);

        return new CustViewAdapter(itemView);
    }

    @Override
    public void onBindViewHolder(StaffmanagementListAdapter.CustViewAdapter holder, int position) {
        QuotationModel customerModel = quoteList.get(position);
        holder.username.setText(customerModel.getCompName());



    }

    @Override
    public int getItemCount() {
        return quoteList.size();
    }
}