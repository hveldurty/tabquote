package tabquote.mycompany.com.myquate.fragments;


import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import tabquote.mycompany.com.myquate.R;
import tabquote.mycompany.com.myquate.adapters.Product_ListAdapter;
import tabquote.mycompany.com.myquate.database.DatabaseHandler;
import tabquote.mycompany.com.myquate.model.products;
import tabquote.mycompany.com.myquate.util.SessionManager;

import static android.content.Context.ACCOUNT_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductCatalogFragment extends Fragment {
    List<products> list;
    DatabaseHandler db;
    Button add;
    EditText productname,productdesc,productprice;
    TextView productcount;
    ListView mListView;
    Product_ListAdapter adapt;
    int product_counts;
    SharedPreferences pref;
    SessionManager session;



    // The authority for the sync adapter's content provider
    public static final String AUTHORITY = "tabquote.mycompany.com.myquate.database";
    // An account type, in the form of a domain name
public static final String ACCOUNT_TYPE = "tabquote.com";
    // The account name
    public static final String ACCOUNT = "dummyaccount";
    // Instance fields
    Account mAccount;

    public static final long SYNC_INTERVAL = 1;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAccount = CreateSyncAccount(getActivity());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
    public ProductCatalogFragment() {
        // Required empty public constructor
    }

    /**
     * Create a new dummy account for the sync adapter
     *
     * @param context The application context
     */
    public static Account CreateSyncAccount(Context context) {
        // Create the account type and default account
        Account newAccount = new Account(
                ACCOUNT, ACCOUNT_TYPE);
        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) context.getSystemService(
                        ACCOUNT_SERVICE);
        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
        if (accountManager.addAccountExplicitly(newAccount, null, null)) {
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call context.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */
        } else {
            /*
             * The account exists or some other error occurred. Log this, report it,
             * or handle it internally.
             */
        }
        return newAccount;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_product_catalog, container, false);
        productname = (EditText) rootView.findViewById(R.id.prodcutname);
        productdesc = (EditText) rootView.findViewById(R.id.prodcutdesc);
        productprice = (EditText) rootView.findViewById(R.id.proprice);
        productcount = (TextView) rootView.findViewById(R.id.productcount);
        add  = (Button) rootView.findViewById(R.id.add_product);
        mListView  = (ListView) rootView.findViewById(R.id.procuctlist);

        pref = getActivity().getSharedPreferences("TabQuotePref", 0);
        session = new SessionManager(getActivity());
         db = new DatabaseHandler(getActivity());
        list = db.getAllContacts();


        adapt = new Product_ListAdapter(getActivity(), R.layout.product_list_layout, list);
        mListView.setAdapter(adapt);
        product_counts = db.getProductCount();
        productcount.setText(getResources().getString(R.string.productcount,String.valueOf(product_counts)));
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkFirstRun();



            }
        });

        return rootView;
    }



    public static ProductCatalogFragment newInstance(String itemID) {
        ProductCatalogFragment fragment = new ProductCatalogFragment();

        return fragment;
    }

    public void checkFirstRun() {
        final Dialog dialog = new Dialog(getActivity());


        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_product_dailog);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);
        dialog.setCanceledOnTouchOutside(false);
        // Window window = dialog.getWindow();
        // window.setLayout(650, 600);
        dialog.getWindow().getAttributes().width = (int) (getDeviceMetrics(getActivity()).widthPixels*0.5);
        dialog.show();

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                // Prevent dialog close on back press button
                return keyCode == KeyEvent.KEYCODE_BACK;
            }
        });
        Button button_gotit = (Button) dialog.findViewById(R.id.button_gotit);
        Button button_no = (Button) dialog.findViewById(R.id.button_back);
        button_gotit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(dialog != null){
                    products task = new products(productname.getText().toString(),productdesc.getText().toString(),
                            productprice.getText().toString());
                    db.addProducts(task);
                    product_counts = db.getProductCount();
                    adapt.add(task);
                    adapt.notifyDataSetChanged();
                    productcount.setText(getResources().getString(R.string.productcount,String.valueOf(product_counts)));
                    productname.setText("");
                    productdesc.setText("");
                    productprice.setText("");


                  //  ContentResolver.setSyncAutomatically(mAccount, AUTHORITY, true);


                    JSONObject post_product = new JSONObject();
                    JSONObject post_finalproduct = new JSONObject();

                    JSONArray array = new JSONArray();


                    try {
                        post_product.put("name" , "iphone7");
                        post_product.put("selling_price", "456666");

                        array.put(post_product);


                        post_finalproduct.put("products" , array);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    if (post_finalproduct.length() > 0) {
                        new SendJsonProductDataToServer().execute(String.valueOf(post_finalproduct));

                    }

                    dialog.dismiss();
                }

            }
        });



        button_no.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(dialog != null){

                    dialog.dismiss();
                }

            }
        });



    }



    class SendJsonProductDataToServer extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... params) {
            String JsonResponse = null;
            String JsonDATA = params[0];
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            String savedtoken = pref.getString("token", "");

            session.getUserDetails();
            try {
                URL url = new URL("http://tabquote.herokuapp.com/api/v1/product_services");
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true);
                // is output buffer writter
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestProperty("AuthToken", "74b3da47d1c07a60481a23448783395a");
                //set headers and method
                Writer writer = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8"));
                writer.write(JsonDATA);
                // json data
                writer.close();
                InputStream inputStream = urlConnection.getInputStream();
                //input stream
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String inputLine;
                while ((inputLine = reader.readLine()) != null)
                    buffer.append(inputLine + "\n");
                if (buffer.length() == 0) {
                    // Stream was empty. No point in parsing.
                    return null;
                }
                JsonResponse = buffer.toString();
                //response data
                Log.i("Server",JsonResponse);
                //send to post execute
                return JsonResponse;




            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e("Server", "Error closing stream", e);
                    }
                }
            }
            return null;

        }


        @Override
        protected void onPostExecute(String s) {
        }


    }

    public static DisplayMetrics getDeviceMetrics(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        display.getMetrics(metrics);
        return metrics;
    }

}
