package tabquote.mycompany.com.myquate.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tabquote.mycompany.com.myquate.BaseFragment;
import tabquote.mycompany.com.myquate.R;

/**
 * Created by M8080375 on 9/26/2016.
 */

public class MyQuotesFragment extends BaseFragment {

    public static final String ARG_ITEM_ID = "item_id";
    private static View rootView;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }
        rootView = inflateAndBind(inflater, container, R.layout.fragment_quotes_layout);
        return rootView;
    }
    public static MyQuotesFragment newInstance(String itemID) {
        MyQuotesFragment fragment = new MyQuotesFragment();
        Bundle args = new Bundle();
        args.putString(MyQuotesFragment.ARG_ITEM_ID, itemID);
        fragment.setArguments(args);
        return fragment;
    }
}
