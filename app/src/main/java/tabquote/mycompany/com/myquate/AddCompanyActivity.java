package tabquote.mycompany.com.myquate;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by kveldurty on 12/26/16.
 */

public class AddCompanyActivity extends Activity implements View.OnClickListener{
    ImageView mRegister;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addcompany_layout);
        mRegister = (ImageView) findViewById(R.id.addbutton);
        mRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.addbutton:
                Intent intent = new Intent(this, AddCompany_DetailsActivity.class);
                startActivity(intent);
            default:
                break;
        }
    }
}
