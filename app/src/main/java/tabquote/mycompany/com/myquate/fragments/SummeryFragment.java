package tabquote.mycompany.com.myquate.fragments;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tabquote.mycompany.com.myquate.R;

/**
 * Created by M8080375 on 9/26/2016.
 */

public class SummeryFragment extends Fragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_summery_layout, container, false);

        return rootView;
    }

    public SummeryFragment() {
    }

    public static SummeryFragment newInstance(String itemID) {
        SummeryFragment fragment = new SummeryFragment();

        return fragment;
    }
}
