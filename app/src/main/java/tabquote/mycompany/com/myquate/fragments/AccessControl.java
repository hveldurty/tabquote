package tabquote.mycompany.com.myquate.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tabquote.mycompany.com.myquate.R;

/**
 * Created by kveldurty on 12/17/16.
 */

public class AccessControl extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
    public AccessControl() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.user_accesscontrol, container, false);
    }

    public static AccessControl newInstance(String itemID) {
        AccessControl fragment = new AccessControl();

        return fragment;
    }

}
