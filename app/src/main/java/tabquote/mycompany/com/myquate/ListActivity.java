package tabquote.mycompany.com.myquate;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;

import tabquote.mycompany.com.myquate.database.DatabaseHandler;
import tabquote.mycompany.com.myquate.dummy.DummyContent;
import tabquote.mycompany.com.myquate.fragments.MyAccountDetailsFragment;
import tabquote.mycompany.com.myquate.fragments.MyCustomerFragment;
import tabquote.mycompany.com.myquate.fragments.MyQuotesFragment;
import tabquote.mycompany.com.myquate.fragments.NavigationDrawerFragment;
import tabquote.mycompany.com.myquate.fragments.SettingsFragment;


/**
 * Lists all available quotes. This Activity supports a single pane (= smartphones) and a two pane mode (= large screens with >= 600dp width).
 *
 * Created by Andreas Schrade on 14.12.2015.
 */
public class ListActivity extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks {
    /**
     * Whether or not the activity is running on a device with a large screen
     */
    private boolean twoPaneMode;
    private Toolbar mToolbar;
    private TextView mTxtEdit;
    private TextView mTxtcloud;
    private TextView mTxtNotify;

    private static final int DASHBOARD = 0;
    private static final int SUPPLIER = 1;
    private static final int CUSTOMER = 2;
    private static final int QUOTES = 3;
    private static final int MYACCOUNT = 4;
    private static final int CALENDER = 5;

    DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout_list);
        db = new DatabaseHandler(this);
        setupToolbar();

        if (isTwoPaneLayoutUsed()) {
            twoPaneMode = true;
        }

        if (savedInstanceState == null && twoPaneMode) {
            setupDetailFragment();
        }


        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            changeTextStatus(true);
        } else {
            changeTextStatus(false);
        }


    }


    private void setupToolbar() {
        final ActionBar ab = getActionBarToolbar();
        //  ab.setHomeAsUpIndicator(R.drawable.ic_menu);
//        ab.setDisplayHomeAsUpEnabled(true);
    }

    private void setupDetailFragment() {
        MyAccountDetailsFragment fragment = MyAccountDetailsFragment.newInstance(DummyContent.ITEMS.get(0).id);
        getFragmentManager().beginTransaction().replace(R.id.article_detail_container, fragment).commit();
    }


    /**
     * Is the container present? If so, we are using the two-pane layout.
     *
     * @return true if the two pane layout is used.
     */
    private boolean isTwoPaneLayoutUsed() {
        return findViewById(R.id.article_detail_container) != null;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        final View menuEdit = menu.findItem(R.id.menu_save).getActionView();
        final View menuCloud = menu.findItem(R.id.menu_cloud).getActionView();
        final View menuCart = menu.findItem(R.id.menu_notify).getActionView();
        final View menuSettings = menu.findItem(R.id.settings).getActionView();

        mTxtEdit = (TextView) menuEdit.findViewById(R.id.txt_edit);
        mTxtcloud = (TextView) menuCloud.findViewById(R.id.txt_cloud);
        mTxtNotify = (TextView) menuCart.findViewById(R.id.txt_notify);
        mTxtEdit.setText("2");
        mTxtcloud.setText("8");
        mTxtNotify.setText("12");
        return super.onCreateOptionsMenu(menu);

    }

    /**
     * Method for initial setup.
     */
    void init() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                openDrawer();
                return true;

            case R.id.settings:
                SettingsFragment fragment1 = SettingsFragment.newInstance(DummyContent.ITEMS.get(0).id);
                getFragmentManager().beginTransaction().replace(R.id.article_detail_container, fragment1).commit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected int getSelfNavDrawerItem() {
        //return R.id.nav_quotes;
        return 0;
    }

    @Override
    public boolean providesActivityToolbar() {
        return true;
    }


    public void changeTextStatus(boolean isConnected) {

        // Change status according to boolean value
        if (isConnected) {

            Log.i("Network", "Connected");
           // syncSQLiteMySQLDB();

            new SendJsonProductDataToServer().execute(String.valueOf(db.convertCursorToJSON()));

        } else {

            Log.i("Network", "NotConnected");

        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        switch (position) {
            case DASHBOARD:
                break;
            case SUPPLIER:
                MyAccountDetailsFragment fragment = MyAccountDetailsFragment.newInstance(DummyContent.ITEMS.get(0).id);
                getFragmentManager().beginTransaction().replace(R.id.article_detail_container, fragment).commit();
                break;
            case CUSTOMER:
                MyCustomerFragment fragmentCustm = MyCustomerFragment.newInstance(DummyContent.ITEMS.get(0).id);
                getFragmentManager().beginTransaction().replace(R.id.article_detail_container, fragmentCustm).commit();
                break;
            case QUOTES:
                MyQuotesFragment fragmentQuote = MyQuotesFragment.newInstance(DummyContent.ITEMS.get(0).id);
                getFragmentManager().beginTransaction().replace(R.id.article_detail_container, fragmentQuote).commit();
                break;
            case MYACCOUNT:
                break;
            case CALENDER:
                break;
            default:
                break;
        }


    }


    class SendJsonProductDataToServer extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... params) {
            String JsonResponse = null;
            String JsonDATA = params[0];
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;


            try {
                URL url = new URL("http://tabquote.herokuapp.com/api/v1/product_services");
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true);
                // is output buffer writter
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestProperty("AuthToken", "74b3da47d1c07a60481a23448783395a");
                //set headers and method
                Writer writer = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8"));
                writer.write(JsonDATA);
                // json data
                writer.close();
                InputStream inputStream = urlConnection.getInputStream();
                //input stream
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String inputLine;
                while ((inputLine = reader.readLine()) != null)
                    buffer.append(inputLine + "\n");
                if (buffer.length() == 0) {
                    // Stream was empty. No point in parsing.
                    return null;
                }
                JsonResponse = buffer.toString();
                //response data
                Log.i("Server",JsonResponse);
                //send to post execute
                return JsonResponse;




            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e("Server", "Error closing stream", e);
                    }
                }
            }
            return null;

        }


        @Override
        protected void onPostExecute(String s) {
        }


    }



}