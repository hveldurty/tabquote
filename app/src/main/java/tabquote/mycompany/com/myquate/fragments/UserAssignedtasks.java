package tabquote.mycompany.com.myquate.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.List;

import tabquote.mycompany.com.myquate.R;
import tabquote.mycompany.com.myquate.adapters.UserGridAdapter;
import tabquote.mycompany.com.myquate.database.DatabaseHandler;
import tabquote.mycompany.com.myquate.dummy.DummyContent;
import tabquote.mycompany.com.myquate.model.products;

/**
 * Created by kveldurty on 12/24/16.
 */



public class UserAssignedtasks extends Fragment {

    List<products> list;
    UserGridAdapter adapt ;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.usertasks_layout, container, false);

        final GridView mListView  = (GridView) rootView.findViewById(R.id.gv_emp);
        final DatabaseHandler db = new DatabaseHandler(getActivity());
        list = db.getAllContacts();


        adapt = new UserGridAdapter(getActivity(), list);
        mListView.setAdapter(adapt);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0:
                        UserSelectedQuote fragment =  UserSelectedQuote.newInstance(DummyContent.ITEMS.get(0).id);

                        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                        transaction.replace(R.id.user_detail_container, fragment).commit();
                        break;


                    default:
                        return;
                }
            }
        });

        return rootView;
    }

    public UserAssignedtasks() {
    }

    public static UserAssignedtasks newInstance(String itemID) {
        UserAssignedtasks fragment = new UserAssignedtasks();

        return fragment;
    }
}
