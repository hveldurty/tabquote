package tabquote.mycompany.com.myquate.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;

import tabquote.mycompany.com.myquate.R;
import tabquote.mycompany.com.myquate.model.QuotationModel;

/**
 * Created by M8080375 on 9/26/2016.
 */

public class MyAccCustFragment extends Fragment {
    private RecyclerView mListCustomer;
    private ArrayList<QuotationModel> custListQuote;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_customer_view, container, false);
        mListCustomer = (RecyclerView) rootView.findViewById(R.id.my_account_cust_lis);
        custListQuote = new ArrayList<QuotationModel>();
        setQuoteListCust();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mListCustomer.setLayoutManager(mLayoutManager);
        mListCustomer.setItemAnimator(new DefaultItemAnimator());
        mListCustomer.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity()).build());
      //  mListCustomer.setAdapter(new MyAccountCustViewAdapter(getActivity(), custListQuote));
        return rootView;
    }

    public MyAccCustFragment() {
    }

    public static MyAccCustFragment newInstance(String itemID) {
        MyAccCustFragment fragment = new MyAccCustFragment();

        return fragment;
    }
    private void setQuoteListCust(){
        QuotationModel mode1 = new QuotationModel();
        mode1.setCompName("Ajith app Kadia");
        mode1.setQuotationdate("12/05/2016");
        mode1.setGrandTotal(1556.65);
        custListQuote.add(mode1);
        QuotationModel mode2 = new QuotationModel();
        mode2.setCompName("Ajith app Kadia");
        mode2.setQuotationdate("12/05/2016");
        mode2.setGrandTotal(1556.65);
        custListQuote.add(mode2);
        QuotationModel mode3 = new QuotationModel();
        mode3.setCompName("Anil app Kadia");
        mode3.setQuotationdate("12/05/2016");
        mode3.setGrandTotal(1556.65);
        custListQuote.add(mode3);
        QuotationModel mode4 = new QuotationModel();
        mode4.setCompName("Suresh app Kadia");
        mode4.setQuotationdate("12/05/2016");
        mode4.setGrandTotal(1556.65);
        custListQuote.add(mode4);

    }

}