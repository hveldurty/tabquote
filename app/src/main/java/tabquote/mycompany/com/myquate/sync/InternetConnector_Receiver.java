package tabquote.mycompany.com.myquate.sync;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import tabquote.mycompany.com.myquate.ListActivity;
import tabquote.mycompany.com.myquate.MyApplication;

/**
 * Created by kveldurty on 12/25/16.
 */

public class InternetConnector_Receiver extends BroadcastReceiver {
    public InternetConnector_Receiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        try {

            boolean isVisible = MyApplication.isActivityVisible();// Check if
            // activity
            // is
            // visible
            // or not
            Log.i("Activity is Visible ", "Is activity visible : " + isVisible);

            // If it is visible then trigger the task else do nothing
           // if (isVisible == true) {
                ConnectivityManager connectivityManager = (ConnectivityManager) context
                        .getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager
                        .getActiveNetworkInfo();

                if (networkInfo != null && networkInfo.isConnected()) {

                   new ListActivity().changeTextStatus(true);
                } else {
                    new ListActivity().changeTextStatus(false);
                }


           // }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}