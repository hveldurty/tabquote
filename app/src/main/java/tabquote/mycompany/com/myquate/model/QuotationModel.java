package tabquote.mycompany.com.myquate.model;

/**
 * Created by M8080375 on 9/27/2016.
 */

public class QuotationModel {

    private int id;
    private String compName;
    private String quotationdate;
    private Double grandTotal;

    public int getId() {
        return id;
    }

    public String getCompName() {
        return compName;
    }

    public String getQuotationdate() {
        return quotationdate;
    }

    public Double getGrandTotal() {
        return grandTotal;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public void setQuotationdate(String quotationdate) {
        this.quotationdate = quotationdate;
    }

    public void setGrandTotal(Double grandTotal) {
        this.grandTotal = grandTotal;
    }
}
