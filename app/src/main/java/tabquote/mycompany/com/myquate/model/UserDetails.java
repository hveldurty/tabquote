package tabquote.mycompany.com.myquate.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDetails implements Parcelable {

	@SerializedName("user_phno")
	@Expose
	private String userPhno;
	@SerializedName("imei_no")
	@Expose
	private String imeiNo;
	@SerializedName("device_model")
	@Expose
	private String deviceModel;
	@SerializedName("os_version")
	@Expose
	private String osVersion;
	@SerializedName("os_name")
	@Expose
	private String osName;
	@SerializedName("app_version")
	@Expose
	private String appVersion;
	@SerializedName("network_agent")
	@Expose
	private String networkAgent;
	@SerializedName("email_id")
	@Expose
	private String emailId;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("lat")
	@Expose
	private String lat;
	@SerializedName("long")
	@Expose
	private String _long;
	@SerializedName("att")
	@Expose
	private String att;
	@SerializedName("otp")
	@Expose
	private String otp;
    public UserDetails(){

    }
	protected UserDetails(Parcel in) {
		userPhno = in.readString();
		imeiNo = in.readString();
		deviceModel = in.readString();
		osVersion = in.readString();
		osName = in.readString();
		appVersion = in.readString();
		networkAgent = in.readString();
		emailId = in.readString();
		name = in.readString();
		lat = in.readString();
		_long = in.readString();
		att = in.readString();
		otp = in.readString();
	}

	public static final Creator<UserDetails> CREATOR = new Creator<UserDetails>() {
		@Override
		public UserDetails createFromParcel(Parcel in) {
			return new UserDetails(in);
		}

		@Override
		public UserDetails[] newArray(int size) {
			return new UserDetails[size];
		}
	};

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	/**
	 * 
	 * @return The userPhno
	 */
	public String getUserPhno() {
		return userPhno;
	}

	/**
	 * 
	 * @param userPhno
	 *            The user_phno
	 */
	public void setUserPhno(String userPhno) {
		this.userPhno = userPhno;
	}

	/**
	 * 
	 * @return The imeiNo
	 */
	public String getImeiNo() {
		return imeiNo;
	}

	/**
	 * 
	 * @param imeiNo
	 *            The imei_no
	 */
	public void setImeiNo(String imeiNo) {
		this.imeiNo = imeiNo;
	}

	/**
	 * 
	 * @return The deviceModel
	 */
	public String getDeviceModel() {
		return deviceModel;
	}

	/**
	 * 
	 * @param deviceModel
	 *            The device_model
	 */
	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}

	/**
	 * 
	 * @return The osVersion
	 */
	public String getOsVersion() {
		return osVersion;
	}

	/**
	 * 
	 * @param osVersion
	 *            The os_version
	 */
	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	/**
	 * 
	 * @return The osName
	 */
	public String getOsName() {
		return osName;
	}

	/**
	 * 
	 * @param osName
	 *            The os_name
	 */
	public void setOsName(String osName) {
		this.osName = osName;
	}

	/**
	 * 
	 * @return The appVersion
	 */
	public String getAppVersion() {
		return appVersion;
	}

	/**
	 * 
	 * @param appVersion
	 *            The app_version
	 */
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	/**
	 * 
	 * @return The networkAgent
	 */
	public String getNetworkAgent() {
		return networkAgent;
	}

	/**
	 * 
	 * @param networkAgent
	 *            The network_agent
	 */
	public void setNetworkAgent(String networkAgent) {
		this.networkAgent = networkAgent;
	}

	/**
	 * 
	 * @return The emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * 
	 * @param emailId
	 *            The email_id
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * 
	 * @return The name
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 *            The name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return The lat
	 */
	public String getLat() {
		return lat;
	}

	/**
	 * 
	 * @param lat
	 *            The lat
	 */
	public void setLat(String lat) {
		this.lat = lat;
	}

	/**
	 * 
	 * @return The _long
	 */
	public String getLong() {
		return _long;
	}

	/**
	 * 
	 * @param _long
	 *            The long
	 */
	public void setLong(String _long) {
		this._long = _long;
	}

	/**
	 * 
	 * @return The att
	 */
	public String getAtt() {
		return att;
	}

	/**
	 * 
	 * @param att
	 *            The att
	 */
	public void setAtt(String att) {
		this.att = att;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(userPhno);
		parcel.writeString(imeiNo);
		parcel.writeString(deviceModel);
		parcel.writeString(osVersion);
		parcel.writeString(osName);
		parcel.writeString(appVersion);
		parcel.writeString(networkAgent);
		parcel.writeString(emailId);
		parcel.writeString(name);
		parcel.writeString(lat);
		parcel.writeString(_long);
		parcel.writeString(att);
		parcel.writeString(otp);
	}

}