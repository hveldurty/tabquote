package tabquote.mycompany.com.myquate.model;

/**
 * Created by M8080375 on 9/20/2016.
 */

public class SortModel {

    private String name;   //Display data
    private String sortLetters;  //Data display Pinyin initials

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSortLetters() {
        return sortLetters;
    }
    public void setSortLetters(String sortLetters) {
        this.sortLetters = sortLetters;
    }
}