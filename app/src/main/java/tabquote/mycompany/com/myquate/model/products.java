package tabquote.mycompany.com.myquate.model;

/**
 * Created by kveldurty on 12/17/16.
 */


public class products {

    public products(String name, String desc, String price) {
        this.productname = name;
        this.productdescription = desc;
        this.productprice = price;
    }
    public products() {
        // TODO Auto-generated constructor stub
    }
    public String getProductname() {
        return productname;
    }
    public void setProductname(String productname) {
        this.productname = productname;
    }
    public String getProductdescription() {
        return productdescription;
    }
    public void setProductdescription(String productdescription) {
        this.productdescription = productdescription;
    }
    public String getProductprice() {
        return productprice;
    }
    public void setProductprice(String productprice) {
        this.productprice = productprice;
    }
    String productname;
    String productdescription;
    String productprice;


}
