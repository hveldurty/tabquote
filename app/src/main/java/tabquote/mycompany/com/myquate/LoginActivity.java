package tabquote.mycompany.com.myquate;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;

import tabquote.mycompany.com.myquate.model.RegesterUser;
import tabquote.mycompany.com.myquate.model.UserDetails;
import tabquote.mycompany.com.myquate.util.SessionManager;


public class LoginActivity extends Activity {
	
	private String entredMobileNo;
	private EditText mEdtUser, mEdtPassword;
    private Button veryfyBtn;
    private RegesterUser registerUser;
	private UserDetails userDetails;

	SharedPreferences pref;
	String JsonResponse = null;
	SessionManager session;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity);
		pref = getApplicationContext().getSharedPreferences("MyPref", 0);
		session = new SessionManager(getApplicationContext());
		mEdtUser = (EditText)findViewById(R.id.username);
		mEdtPassword = (EditText)findViewById(R.id.password);
		 veryfyBtn=(Button)findViewById(R.id.login);

		 registerUser =  new RegesterUser();
		 registerUser.setServiceId("1");
		 registerUser.setSubServiceId("11");

		 veryfyBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {


				JSONObject post_dict = new JSONObject();

				try {
					post_dict.put("email" , "mjayakumar2k6@gmail.com");
					post_dict.put("password", "password");


				} catch (JSONException e) {
					e.printStackTrace();
				}
				if (post_dict.length() > 0) {
					new SendJsonDataToServer().execute(String.valueOf(post_dict));

				}



				Intent intent = new Intent(LoginActivity.this, AddCompanyActivity.class);
				startActivity(intent);

				
			}


		});
	}




	class SendJsonDataToServer extends AsyncTask <String,String,String> {

		@Override
		protected String doInBackground(String... params) {

			String JsonDATA = params[0];
			HttpURLConnection urlConnection = null;
			BufferedReader reader = null;
			try {
				URL url = new URL("http://tabquote.herokuapp.com/api/v1/login");
				urlConnection = (HttpURLConnection) url.openConnection();
				urlConnection.setDoOutput(true);
				// is output buffer writter
				urlConnection.setRequestMethod("POST");
				urlConnection.setRequestProperty("Content-Type", "application/json");
				urlConnection.setRequestProperty("Accept", "application/json");
				urlConnection.setRequestProperty("REMOTE_ADDR", "123");
				//set headers and method
				Writer writer = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8"));
				writer.write(JsonDATA);
				// json data
				writer.close();
				InputStream inputStream = urlConnection.getInputStream();
				//input stream
				StringBuffer buffer = new StringBuffer();
				if (inputStream == null) {
					// Nothing to do.
					return null;
				}
				reader = new BufferedReader(new InputStreamReader(inputStream));

				String inputLine;
				while ((inputLine = reader.readLine()) != null)
					buffer.append(inputLine + "\n");
				if (buffer.length() == 0) {
					// Stream was empty. No point in parsing.
					return null;
				}
				JsonResponse = buffer.toString();
				//response data
				Log.i("Server", JsonResponse);
				//send to post execute
				return JsonResponse;


			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (urlConnection != null) {
					urlConnection.disconnect();
				}
				if (reader != null) {
					try {
						reader.close();
					} catch (final IOException e) {
						Log.e("Server", "Error closing stream", e);
					}
				}
			}


			try {
				JSONObject json = new JSONObject(JsonResponse);


				// get value from LL Json Object
				String token_value = json.getString("token");
				Log.i("token", token_value);
				session.createLoginSession("password", "mjayakumar2k6@gmail.com", token_value);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;

		}


		@Override
		protected void onPostExecute(String s) {
			try {
				JSONObject json = new JSONObject(JsonResponse);


				// get value from LL Json Object
				String str_value = json.getString("token");
				Log.i("token", str_value);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		
		return super.onOptionsItemSelected(item);
	}
}
