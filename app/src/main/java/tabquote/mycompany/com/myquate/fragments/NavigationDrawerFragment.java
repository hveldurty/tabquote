
package tabquote.mycompany.com.myquate.fragments;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import tabquote.mycompany.com.myquate.R;
import tabquote.mycompany.com.myquate.dummy.DummyContent;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 */
public class NavigationDrawerFragment extends Fragment implements OnClickListener{
	
	/**
	 * String constant of preference name to identify that the user has understood
	 * about NavigationDrawer.
	 */
	private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";
	/**
	 * String constant of preference name to save the selected position of 
	 * NavigationDrawer.
	 */
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";
    /**
     * Preference name
     */
    private static final String PREFERENCES_FILE = "my_app_settings"; 
    /**
     * Container view which holds the fragments.
     */
    private View mFragmentContainerView;
    /**
     * DrawerLayout reference.
     */
    private DrawerLayout mDrawerLayout;
    /**
     * ActionBarToggle reference.
     */
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    /**
     * Boolean variable to identify whether the user learned about 
     * NavigationDrawer.
     */
    private boolean mUserLearnedDrawer;
    /**
     * Boolean variable to identify whether anything is saved in the bundle.
     */
    private boolean mFromSavedInstanceState;
    /**
     * Integer variable to save the selected position of NavigationDrawer.
     */
    private int mCurrentSelectedPosition = 0;

    /**
     * A pointer to the current callbacks instance (the Activity).
     */
    private NavigationDrawerCallbacks mCallbacks;
    private ListView mDrawerListViewTop;
    
    /**
     * Constructor of NavigationDrawerFragment class.
     */
    public NavigationDrawerFragment() {
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mUserLearnedDrawer = Boolean.valueOf(readSharedSetting(getActivity(), PREF_USER_LEARNED_DRAWER, "false"));
        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mFromSavedInstanceState = true;
        }
        
        // Select either the default item (0) or the last selected item.
        selectItem(mCurrentSelectedPosition);
        
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
    		Bundle savedInstanceState) {
    	// TODO Auto-generated method stub
    	View rootView = (View)inflater.inflate(
                R.layout.side_bar_list, container, false);


        mDrawerListViewTop = (ListView)rootView.findViewById(R.id._navigation_list);

        mDrawerListViewTop.setAdapter(new MyListAdapter());
        mDrawerListViewTop.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //if(position != 0){
                selectItem(position);
                //}

            }
        });

        return rootView;
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }
    

    public ActionBarDrawerToggle getActionBarDrawerToggle() {
        return mActionBarDrawerToggle;
    }

    public void setActionBarDrawerToggle(ActionBarDrawerToggle actionBarDrawerToggle) {
        mActionBarDrawerToggle = actionBarDrawerToggle;
    }
    
    /**
     * Method to do the initial setup
     * @param fragmentId
     * @param drawerLayout
     * @param toolbar
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout, Toolbar toolbar) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        /*if(mFragmentContainerView.getParent() instanceof ScrimInsetsFrameLayout){
            mFragmentContainerView = (View) mFragmentContainerView.getParent();
        }*/

        if (!mUserLearnedDrawer && !mFromSavedInstanceState)
            mDrawerLayout.openDrawer(mFragmentContainerView);

        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mActionBarDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);
    }

    /**
     * Method to open the NavigationDrawer
     */
    public void openDrawer() {
        mDrawerLayout.openDrawer(mFragmentContainerView);
    }

    /**
     * Method to close the NavigationDrawer
     */
    public void closeDrawer() {
        mDrawerLayout.closeDrawer(mFragmentContainerView);
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }
    
    /**
     * Changes the icon of the drawer to back
     */
    public void showBackButton() {
        if (getActivity() instanceof AppCompatActivity) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * Changes the icon of the drawer to menu
     */
    public void showDrawerButton() {
        if (getActivity() instanceof AppCompatActivity) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        mActionBarDrawerToggle.syncState();
    }
    
    /**
     * Method to notify  the activity on selected position in NavigationDrawer
     * using callbacks.
     * @param position
     */
    void selectItem(int position) {
        mCurrentSelectedPosition = position;
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }
        /* ((NavigationDrawerAdapter) mDrawerList.getAdapter()).selectPosition(position);*/
    }

    /**
     * Method to check whether the NavigationDrawer is opened or not.
     * @return
     */
    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mActionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }
    
    /**
     * Method to get the DrawerLayout instance.
     * @return
     */
    public DrawerLayout getDrawerLayout() {
        return mDrawerLayout;
    }

    /**
     * Method to set the DrawerLayout instance.
     * @param drawerLayout
     */
    public void setDrawerLayout(DrawerLayout drawerLayout) {
        mDrawerLayout = drawerLayout;
    }
    
    /**
     * Method to save the preference value.
     * @param ctx
     * @param settingName
     * @param settingValue
     */
    public static void saveSharedSetting(Context ctx, String settingName, String settingValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(settingName, settingValue);
        editor.apply();
    }

    /**
     * Method to get the preference value.
     * @param ctx
     * @param settingName
     * @param defaultValue
     * @return
     */
    public static String readSharedSetting(Context ctx, String settingName, String defaultValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        return sharedPref.getString(settingName, defaultValue);
    }
    
    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public static interface NavigationDrawerCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onNavigationDrawerItemSelected(int position);
    }

	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {

		default:
			break;
		}
		
	}
    private class MyListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return DummyContent.ITEMS.size();
        }

        @Override
        public Object getItem(int position) {
            return DummyContent.ITEMS.get(position);
        }

        @Override
        public long getItemId(int position) {
            return DummyContent.ITEMS.get(position).id.hashCode();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup container) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.list_item_article, container, false);
            }
            final DummyContent.DummyItem item = (DummyContent.DummyItem) getItem(position);
            ((TextView) convertView.findViewById(R.id.article_title)).setText(item.title);

            final ImageView img = (ImageView) convertView.findViewById(R.id.thumbnail);
            Picasso.with(getActivity())
                    .load(item.photoId).into(img);

            return convertView;
        }
        private class ViewHolder
        {
            TextView mDescription;
            ImageView icon;
        }
    }

}
