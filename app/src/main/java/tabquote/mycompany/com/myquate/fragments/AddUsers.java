package tabquote.mycompany.com.myquate.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;

import tabquote.mycompany.com.myquate.R;
import tabquote.mycompany.com.myquate.database.DatabaseHandler;
import tabquote.mycompany.com.myquate.model.Users;

/**
 * Created by kveldurty on 12/17/16.
 */

public class AddUsers extends Fragment {
 Button addtodatabase;
    DatabaseHandler db;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
    public AddUsers() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.adduserfragment,
                container, false);
        addtodatabase = (Button) rootView.findViewById(R.id.addtodatabase);
        db = new DatabaseHandler(getActivity());


        addtodatabase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                db.addContact(new Users(1,"Ravi", "Developer"));




            }
        });




        return rootView;
    }

    public static AddUsers newInstance(String itemID) {
        AddUsers fragment = new AddUsers();

        return fragment;
    }

}
