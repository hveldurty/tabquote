package tabquote.mycompany.com.myquate.model;

/**
 * Created by M8080375 on 9/21/2016.
 */

public class CustomerModel implements Comparable<CustomerModel>{
    private String compName;
    private String contactPerson;
    private String address;
    private String address1;
    private String address2;

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCompName() {
        return compName;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public String getAddress() {
        return address;
    }

    public String getAddress1() {
        return address1;
    }

    public String getAddress2() {
        return address2;
    }

    public int compareTo(CustomerModel other) {
        return compName.compareTo(other.compName);
    }
}
