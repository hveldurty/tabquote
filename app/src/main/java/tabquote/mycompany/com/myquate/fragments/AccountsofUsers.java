package tabquote.mycompany.com.myquate.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tabquote.mycompany.com.myquate.R;


public class AccountsofUsers extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.user_accounts_layout, container, false);



        return rootView;
    }

    public AccountsofUsers() {
    }

    public static AccountsofUsers newInstance(String itemID) {
        AccountsofUsers fragment = new AccountsofUsers();

        return fragment;
    }
}