package tabquote.mycompany.com.myquate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import tabquote.mycompany.com.myquate.R;
import tabquote.mycompany.com.myquate.model.products;

public class UserGridAdapter extends BaseAdapter {
	Context context;
	List<products> empList;
	private static LayoutInflater inflater = null;

	public UserGridAdapter(Context context, List<products> empList) {
	    this.context = context;
	    this.empList = empList;
	    inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
	    // TODO Auto-generated method stub
	    return empList.size();
	}

	@Override
	public Object getItem(int position) {
	    // TODO Auto-generated method stub
	    return position;
	}

	@Override
	public long getItemId(int position) {
	    // TODO Auto-generated method stub
	    return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	    // TODO Auto-generated method stub
	    if (convertView == null)
	        convertView = inflater.inflate(R.layout.gridview_layout, null);

	    TextView logoortitle = (TextView) convertView.findViewById(R.id.logoortitle);
	    TextView location = (TextView) convertView.findViewById(R.id.location);
	    

	    products e = new products();
	    e = empList.get(position);
	    logoortitle.setText(String.valueOf(e.getProductname()));
	    location.setText(String.valueOf(e.getProductprice()));
	    
	    return convertView;
	}

	}