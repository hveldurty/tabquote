package tabquote.mycompany.com.myquate.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tabquote.mycompany.com.myquate.R;


/**
 * Just dummy content. Nothing special.
 *
 * Created by Andreas Schrade on 14.12.2015.
 */
public class DummyContent {

    /**
     * An array of sample items.
     */
    public static final List<DummyItem> ITEMS = new ArrayList<>();
    public static final List<DummyItem> USERITEMS = new ArrayList<>();
    public static final List<MyAccountTabs> ACCOUNT_TABS = new ArrayList<>();
    public static final List<MyProductTabs> PRODUCT_TABS = new ArrayList<>();
    public static final List<MyUserTabs> USER_TABS = new ArrayList<>();
    /**
     * A map of sample items. Key: sample ID; Value: Item.
     */
    public static final Map<String, DummyItem> ITEM_MAP = new HashMap<>(5);

    static {
        addItem(new DummyItem("1",R.drawable.dashboard, "Dashboard"));
        addItem(new DummyItem("2", R.drawable.transport, "My Suppliers"));
        addItem(new DummyItem( "3", R.drawable.group, "My Customers"));
        addItem(new DummyItem( "4",R.drawable.quote, "My Quotes"));
        addItem(new DummyItem("5",R.drawable.key, "My Keys"));
        addItem(new DummyItem("6",R.drawable.calendar, "Calender"));
    }

    public static final Map<String, DummyItem> USERITEM_MAP = new HashMap<>(3);

    static {
        adduserItem(new DummyItem("1",R.drawable.dashboard, "Assigned Tasks"));
        adduserItem(new DummyItem("2", R.drawable.transport, "Quotes"));
        adduserItem(new DummyItem( "3", R.drawable.group, "Accounts"));


    }

    public static final Map<String, MyAccountTabs> ACCOUNT_TABS_MAP = new HashMap<>(5);
    static {
        addTabItem(new MyAccountTabs("1","Summery"));
        addTabItem(new MyAccountTabs("2", "Customer view"));
        addTabItem(new MyAccountTabs( "3", "Cash flow"));
        addTabItem(new MyAccountTabs( "4", "Important Dates"));
    }

    public static final Map<String, MyProductTabs> PRODUCT_TABS_MAP = new HashMap<>(2);
    static {
        addTabItem(new MyProductTabs("1","Product Catalog"));

    }

    public static final Map<String, MyUserTabs> USER_TABS_MAP = new HashMap<>(4);
    static {
        addTabItem(new MyUserTabs("1","Create"));
        addTabItem(new MyUserTabs("2", "Access Control"));
        addTabItem(new MyUserTabs( "3", "Staff Management"));

    }

    private static void addItem(DummyItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }
    private static void adduserItem(DummyItem item) {
        USERITEMS.add(item);
        USERITEM_MAP.put(item.id, item);
    }
    private static void addTabItem(MyAccountTabs item) {
        ACCOUNT_TABS.add(item);
        ACCOUNT_TABS_MAP.put(item.id, item);

    }

    private static void addTabItem(MyProductTabs item) {
        PRODUCT_TABS.add(item);
        PRODUCT_TABS_MAP.put(item.id, item);

    }

    private static void addTabItem(MyUserTabs item) {
        USER_TABS.add(item);
        USER_TABS_MAP.put(item.id, item);

    }

    public static class DummyItem {
        public final String id;
        public final int photoId;
        public final String title;


        public DummyItem(String id, int photoId, String title) {
            this.id = id;
            this.photoId = photoId;
            this.title = title;
        }
    }
    public static class MyAccountTabs {
        public final String id;
        public final String title;


        public MyAccountTabs(String id, String title) {
            this.id = id;
            this.title = title;
        }
    }

    public static class MyProductTabs {
        public final String id;
        public final String title;


        public MyProductTabs(String id, String title) {
            this.id = id;
            this.title = title;
        }
    }

    public static class MyUserTabs {
        public final String id;
        public final String title;


        public MyUserTabs(String id, String title) {
            this.id = id;
            this.title = title;
        }
    }
}
