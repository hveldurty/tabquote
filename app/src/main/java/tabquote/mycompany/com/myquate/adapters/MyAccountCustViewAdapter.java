package tabquote.mycompany.com.myquate.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import tabquote.mycompany.com.myquate.R;
import tabquote.mycompany.com.myquate.model.QuotationModel;

/**
 * Created by M8080375 on 9/27/2016.
 */


public class MyAccountCustViewAdapter extends RecyclerView.Adapter<MyAccountCustViewAdapter.CustViewAdapter> {

    private ArrayList<QuotationModel> quoteList;
    private Context mContext;
    public class CustViewAdapter extends RecyclerView.ViewHolder {
        public TextView name, quoteDate, grandTotal;
        public ImageView custImage;

        public CustViewAdapter(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.customer_name);
            quoteDate = (TextView) view.findViewById(R.id.quoteDate);
            grandTotal = (TextView) view.findViewById(R.id.grand_total);
            custImage = (ImageView) view.findViewById(R.id.customer_img);
        }
    }


    public MyAccountCustViewAdapter(Context context, ArrayList<QuotationModel> custList) {
        this.quoteList = custList;
        this.mContext = context;
    }

    @Override
    public CustViewAdapter onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.customer_list_item, parent, false);

        return new CustViewAdapter(itemView);
    }

    @Override
    public void onBindViewHolder(CustViewAdapter holder, int position) {
        QuotationModel customerModel = quoteList.get(position);
        holder.name.setText(customerModel.getCompName());
        holder.quoteDate.setText("Quotation Sent "+customerModel.getQuotationdate());
        holder.grandTotal.setText(customerModel.getGrandTotal().toString());
        if(position == 2){
            holder.grandTotal.setBackground(mContext.getResources().getDrawable(R.drawable.green_price_bg));
        }
    }

    @Override
    public int getItemCount() {
        return quoteList.size();
    }
}