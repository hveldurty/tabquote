package tabquote.mycompany.com.myquate.model;

/**
 * Created by kveldurty on 12/17/16.
 */

public class Users {

    int userid;

    public int getUserid() {
        return userid;
    }

    public Users() {
    }

    public Users(int userid, String name, String role) {
        this.userid = userid;
        this.name = name;
        this.role = role;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    String name;
    String role;
}
