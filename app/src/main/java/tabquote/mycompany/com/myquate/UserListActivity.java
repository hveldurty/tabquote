package tabquote.mycompany.com.myquate;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import tabquote.mycompany.com.myquate.dummy.DummyContent;
import tabquote.mycompany.com.myquate.fragments.AccountsofUsers;
import tabquote.mycompany.com.myquate.fragments.UserAssignedtasks;
import tabquote.mycompany.com.myquate.fragments.UserNavigationDrawerFragment;
import tabquote.mycompany.com.myquate.fragments.UserSelectedQuote;

/**
 * Created by kveldurty on 12/21/16.
 */

public class UserListActivity extends BaseActivity implements UserNavigationDrawerFragment.NavigationDrawerCallbacks {
    /**
     * Whether or not the activity is running on a device with a large screen
     */
    private boolean twoPaneMode;
    private Toolbar mToolbar;



    private static final int  ASSIGNEDTASKS = 0;
    private static final int QUOTES = 1;
    private static final int ACCOUNTS = 2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_activity_list);

        setupToolbar();

        if (isTwoPaneLayoutUsed()) {
            twoPaneMode = true;
        }

        if (savedInstanceState == null && twoPaneMode) {
            setupDetailFragment();
        }
    }



    private void setupToolbar() {
        final ActionBar ab = getActionBarToolbar();
        //  ab.setHomeAsUpIndicator(R.drawable.ic_menu);
//        ab.setDisplayHomeAsUpEnabled(true);
    }

    private void setupDetailFragment() {
        UserAssignedtasks fragment =  UserAssignedtasks.newInstance(DummyContent.ITEMS.get(0).id);
        getFragmentManager().beginTransaction().replace(R.id.user_detail_container, fragment).commit();
    }



    /**
     * Is the container present? If so, we are using the two-pane layout.
     *
     * @return true if the two pane layout is used.
     */
    private boolean isTwoPaneLayoutUsed() {
        return findViewById(R.id.user_detail_container) != null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.usermenu, menu);
        final View menuUser = menu.findItem(R.id.user).getActionView();

        return super.onCreateOptionsMenu(menu);

    }
    /**
     * Method for initial setup.
     */
    void init(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /*case R.id.user:
                SettingsFragment fragment1 =  SettingsFragment.newInstance(DummyContent.ITEMS.get(0).id);
                getFragmentManager().beginTransaction().replace(R.id.user_detail_container, fragment1).commit();
                return true;*/

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected int getSelfNavDrawerItem() {
        //return R.id.nav_quotes;
        return 0;
    }

    @Override
    public boolean providesActivityToolbar() {
        return true;
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        switch (position){
            case ASSIGNEDTASKS:
                UserAssignedtasks fragmentassignedtask =  UserAssignedtasks.newInstance(DummyContent.ITEMS.get(0).id);
                getFragmentManager().beginTransaction().replace(R.id.user_detail_container, fragmentassignedtask).commit();
                break;
            case QUOTES:
                UserSelectedQuote fragmentuserquote =  UserSelectedQuote.newInstance(DummyContent.ITEMS.get(0).id);
                getFragmentManager().beginTransaction().replace(R.id.user_detail_container, fragmentuserquote).commit();
                break;
            case ACCOUNTS:
                AccountsofUsers fragmentuseracounts =  AccountsofUsers.newInstance(DummyContent.ITEMS.get(0).id);
                getFragmentManager().beginTransaction().replace(R.id.user_detail_container, fragmentuseracounts).commit();
                break;

            default:
                break;
        }

    }
}
