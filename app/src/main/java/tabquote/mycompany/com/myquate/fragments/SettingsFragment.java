package tabquote.mycompany.com.myquate.fragments;

import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import tabquote.mycompany.com.myquate.BaseFragment;
import tabquote.mycompany.com.myquate.R;
import tabquote.mycompany.com.myquate.dummy.DummyContent;

public class SettingsFragment extends BaseFragment {

    /**
     * The argument represents the dummy item ID of this fragment.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The dummy content of this fragment.
     */
    private DummyContent.DummyItem dummyItem;
    private ListView mListTabs,mListUserTabs;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // load dummy item by using the passed item ID.
            dummyItem = DummyContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
        }

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.settings_layout, container, false);


        ProductCatalogFragment fragment =  ProductCatalogFragment.newInstance(DummyContent.ITEMS.get(0).id);

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.settings_container, fragment).commit();


        mListTabs = (ListView) rootView.findViewById(R.id.settingslist);
       // mListUserTabs = (ListView) rootView.findViewById(R.id.userlist);
       // mListTabs.setAdapter(new MyListAdapter());
       // mListUserTabs.setAdapter(new MyListUserAdapter());


        Person ProductCatalog = new Person("Product Catalog", "123 Fake Dr.");
        Person create = new Person("Create", "456 Unreal Ln.");
        Person access = new Person("Access Control", "147 Seashell Place");
        Person staff = new Person("Staff Management", "135 Bayside Ct.");
        Person notify = new Person("Notifications", "246 Bowser Castle");
        Person news = new Person("Weather & News", "7911 Peach St.");
        Person themes = new Person("Color Themes", "81012 Blue Blvd");

        ArrayList<Object> people = new ArrayList<>();
        people.add("Product");
        people.add(ProductCatalog);
        people.add("Users");
        people.add(create);
        people.add(access);
        people.add(staff);
        people.add("Other Settings");
        people.add(notify);
        people.add(news);
        people.add(themes);
        mListTabs.setAdapter(new PersonAdapter(getActivity(), people));

        mListTabs.setSelection(1);
        mListTabs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d("Item",  String.valueOf(i));
                switch (i){
          /*          case 1:
                        ProductCatalogFragment fragment =  ProductCatalogFragment.newInstance(DummyContent.ITEMS.get(0).id);

                        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                        transaction.replace(R.id.settings_container, fragment).commit();
                        break;*/
                   case 3:
                        AddUsers fragment1 =  AddUsers.newInstance(DummyContent.ITEMS.get(0).id);

                        FragmentTransaction transaction1 = getChildFragmentManager().beginTransaction();
                        transaction1.replace(R.id.settings_container, fragment1).commit();
                        break;
                    case 4:
                        AccessControl fragment2 =  AccessControl.newInstance(DummyContent.ITEMS.get(0).id);

                        FragmentTransaction transaction2 = getChildFragmentManager().beginTransaction();
                        transaction2.replace(R.id.settings_container, fragment2).commit();
                        break;
                    case 5:
                        staffManagementFragment fragment3 =  staffManagementFragment.newInstance(DummyContent.ITEMS.get(0).id);

                        FragmentTransaction transaction3 = getChildFragmentManager().beginTransaction();
                        transaction3.replace(R.id.settings_container, fragment3).commit();
                    default:
                        return;
                }
            }
        });

        return rootView;
    }

    private void loadBackdrop() {
        //   Glide.with(this).load(dummyItem.photoId).centerCrop().into(backdropImg);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
       /* inflater.inflate(R.menu.sample_actions, menu);*/
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //case R.id.action_settings:
            case 0:    // your logic
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static SettingsFragment newInstance(String itemID) {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        args.putString(SettingsFragment.ARG_ITEM_ID, itemID);
        fragment.setArguments(args);
        return fragment;
    }

    public SettingsFragment() {}

    private class MyListAdapter extends BaseAdapter {


        @Override
        public int getCount() {
            return DummyContent.PRODUCT_TABS.size();
        }

        @Override
        public Object getItem(int position) {
            return DummyContent.PRODUCT_TABS.get(position);
        }

        @Override
        public long getItemId(int position) {
            return DummyContent.PRODUCT_TABS.get(position).id.hashCode();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup container) {

            if (convertView == null) {
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.myaccount_list_item, container, false);
            }
            final DummyContent.MyProductTabs item = (DummyContent.MyProductTabs) getItem(position);
            ((TextView) convertView.findViewById(R.id.list_item)).setText(item.title);


            return convertView;
        }
    }


    private class MyListUserAdapter extends BaseAdapter {


        @Override
        public int getCount() {
            return DummyContent.USER_TABS.size();
        }

        @Override
        public Object getItem(int position) {
            return DummyContent.USER_TABS.get(position);
        }

        @Override
        public long getItemId(int position) {
            return DummyContent.USER_TABS.get(position).id.hashCode();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup container) {

            if (convertView == null) {
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.myaccount_list_item, container, false);
            }
            final DummyContent.MyUserTabs item = (DummyContent.MyUserTabs) getItem(position);
            ((TextView) convertView.findViewById(R.id.list_item)).setText(item.title);


            return convertView;
        }
    }


    public class PersonAdapter extends BaseAdapter {
        private ArrayList<Object> personArray;
        private LayoutInflater inflater;
        private static final int TYPE_PERSON = 0;
        private static final int TYPE_DIVIDER = 1;

        public PersonAdapter(Context context, ArrayList<Object> personArray) {
            this.personArray = personArray;
            this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return personArray.size();
        }

        @Override
        public long getItemId(int position) {
            return personArray.get(position).hashCode();
        }

        @Override
        public Object getItem(int position) {
            return personArray.get(position);
        }

        @Override
        public int getViewTypeCount() {
            // TYPE_PERSON and TYPE_DIVIDER
            return 2;
        }

        @Override
        public int getItemViewType(int position) {
            if (getItem(position) instanceof Person) {
                return TYPE_PERSON;
            }

            return TYPE_DIVIDER;
        }

        @Override
        public boolean isEnabled(int position) {
            return (getItemViewType(position) == TYPE_PERSON);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            int type = getItemViewType(position);
            if (convertView == null) {
                switch (type) {
                    case TYPE_PERSON:
                        convertView = inflater.inflate(R.layout.row_item, parent, false);
                        break;
                    case TYPE_DIVIDER:
                        convertView = inflater.inflate(R.layout.row_item, parent, false);
                        break;
                }
            }

            switch (type) {
                case TYPE_PERSON:
                    Person person = (Person)getItem(position);
                    TextView name = (TextView)convertView.findViewById(R.id.textView1);
                    //TextView address = (TextView)convertView.findViewById(R.id.addressLabel);
                    name.setText(person.getName());
                    name.setPadding(20,0,0,0);
                    //address.setText(person.getAddress());
                    break;
                case TYPE_DIVIDER:
                    TextView title = (TextView)convertView.findViewById(R.id.textView1);
                    String titleString = (String)getItem(position);
                    title.setText(titleString);
                    title.setTypeface(null, Typeface.BOLD);

                    break;
            }

            return convertView;
        }
    }

    public class Person {
        private String name;
        private String address;

        public Person(String name, String address) {
            this.name = name;
            this.address = address;
        }

        public String getName() {
            return name;
        }

        public String getAddress() {
            return address;
        }
    }

}
