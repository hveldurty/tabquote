package tabquote.mycompany.com.myquate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import tabquote.mycompany.com.myquate.R;
import tabquote.mycompany.com.myquate.model.products;

/**
 * Created by kveldurty on 12/19/16.
 */


public class Product_ListAdapter extends ArrayAdapter<products> {

    Context context;
    List<products> taskList = new ArrayList<products>();
    int layoutResourceId;

    public Product_ListAdapter(Context context, int layoutResourceId,
                     List<products> objects) {
        super(context, layoutResourceId, objects);
        this.layoutResourceId = layoutResourceId;
        this.taskList = objects;
        this.context = context;
    }

    /**
     * This method will DEFINe what the view inside the list view will
     * finally look like Here we are going to code that the checkbox state
     * is the status of task and check box text is the task name
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = null;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.product_list_layout, null);
        } else {
            itemView = convertView;
        }

        products current = taskList.get(position);
        TextView textView1 = (TextView) itemView.findViewById(R.id.productname);
        TextView textView2 = (TextView) itemView.findViewById(R.id.productdescription);
        TextView textView3 = (TextView) itemView.findViewById(R.id.productprice);
        TextView textView4 = (TextView) itemView.findViewById(R.id.serialno);

        textView1.setText(current.getProductname());
        textView2.setText(current.getProductdescription());
        textView3.setText(current.getProductprice());
        textView4.setText(String.valueOf(getItemId(position)+1));
        return itemView;
    }

}