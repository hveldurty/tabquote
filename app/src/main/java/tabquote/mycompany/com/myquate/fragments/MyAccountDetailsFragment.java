package tabquote.mycompany.com.myquate.fragments;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import tabquote.mycompany.com.myquate.BaseActivity;
import tabquote.mycompany.com.myquate.BaseFragment;
import tabquote.mycompany.com.myquate.R;
import tabquote.mycompany.com.myquate.TextProgressBar;
import tabquote.mycompany.com.myquate.dummy.DummyContent;


/**
 * Shows the quote detail page.
 *
 * Created by Andreas Schrade on 14.12.2015.
 */
public class MyAccountDetailsFragment extends BaseFragment {

    /**
     * The argument represents the dummy item ID of this fragment.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The dummy content of this fragment.
     */
    private DummyContent.DummyItem dummyItem;
    private ListView mListTabs;
    private TextProgressBar mProgressBar;

/*    @Bind(R.id.backdrop)
    ImageView backdropImg;

    @Bind(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // load dummy item by using the passed item ID.
            dummyItem = DummyContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
        }

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflateAndBind(inflater, container, R.layout.fragment_article_detail);

        if (!((BaseActivity) getActivity()).providesActivityToolbar()) {
            // No Toolbar present. Set include_toolbar:
            ((BaseActivity) getActivity()).setToolbar((Toolbar) rootView.findViewById(R.id.toolbar));
        }
        mProgressBar = new TextProgressBar(getActivity());
        mProgressBar = (TextProgressBar) rootView.findViewById(R.id.progressBar2);
        mProgressBar.setProgress(20);
        mProgressBar.setText(20+"/100");

        mListTabs = (ListView) rootView.findViewById(R.id.my_account_tabs);
        mListTabs.setAdapter(new MyListAdapter());
        if (dummyItem != null) {
            loadBackdrop();

        }
        mListTabs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                switch (position){
                    case 0:
                        SummeryFragment fragment =  SummeryFragment.newInstance(DummyContent.ITEMS.get(0).id);

                        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                        transaction.replace(R.id.myaccount_container, fragment).commit();
                        break;
                    case 1:
                        MyAccCustFragment fragmentView =  MyAccCustFragment.newInstance(DummyContent.ITEMS.get(0).id);

                        FragmentTransaction transactionView = getChildFragmentManager().beginTransaction();
                        transactionView.replace(R.id.myaccount_container, fragmentView).commit();
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    default:
                        return;
                }
            }
        });
        SummeryFragment fragment =  SummeryFragment.newInstance(DummyContent.ITEMS.get(0).id);

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.myaccount_container, fragment).commit();
        return rootView;
    }

    private void loadBackdrop() {
     //   Glide.with(this).load(dummyItem.photoId).centerCrop().into(backdropImg);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
       /* inflater.inflate(R.menu.sample_actions, menu);*/
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //case R.id.action_settings:
            case 0:    // your logic
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static MyAccountDetailsFragment newInstance(String itemID) {
        MyAccountDetailsFragment fragment = new MyAccountDetailsFragment();
        Bundle args = new Bundle();
        args.putString(MyAccountDetailsFragment.ARG_ITEM_ID, itemID);
        fragment.setArguments(args);
        return fragment;
    }

    public MyAccountDetailsFragment() {}


    private class MyListAdapter extends BaseAdapter{


        @Override
        public int getCount() {
            return DummyContent.ACCOUNT_TABS.size();
        }

        @Override
        public Object getItem(int position) {
            return DummyContent.ACCOUNT_TABS.get(position);
        }

        @Override
        public long getItemId(int position) {
            return DummyContent.ACCOUNT_TABS.get(position).id.hashCode();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup container) {

            if (convertView == null) {
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.myaccount_list_item, container, false);
            }
            final DummyContent.MyAccountTabs item = (DummyContent.MyAccountTabs) getItem(position);
            ((TextView) convertView.findViewById(R.id.list_item)).setText(item.title);


            return convertView;
        }
    }
}
